import executor.CommandExecutor;
import executor.Executor;
import system_boundary.CommandCreator;
import view.ApplicationView;

import java.util.List;

public class Application {

    ApplicationView view;

    public Application() {
        List<CommandCreator> commandCreatorList = new Loader<CommandCreator>().load(CommandCreator.class);
        view = new ApplicationView(new CommandExecutor(commandCreatorList));
        commandCreatorList.forEach(creator->creator.attachObserver(view));
    }

    public void run() {
        do {
            view.display();
        } while (true);
    }

    public static void main(String [] args) {
        new Application().run();
    }
}