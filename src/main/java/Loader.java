import system_boundary.CommandCreator;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ServiceLoader;

public class Loader <T> {

    private Object K;

    public List<T> load(Class<T> service) {
        ServiceLoader<T> serviceLoader = ServiceLoader.load(service);
        Iterator<T> it = serviceLoader.iterator();
        LinkedList<T> commandCreators = new LinkedList<>();
        while(it.hasNext()) {
            commandCreators.add(it.next());
        }
        return commandCreators;
    }


}
